#include <iostream>
#include <string>
#include <map>
#include <regex>
using namespace std;

// struct question-answer
map<string, string> questions_answers = {
  {"hello", "hey there, human"},
  {"how do you do", "I'm okay"},
  {"what is your.*name", "jarv"},
  {"say the link", "https://youtu.be/ymY4E4eG7KQ"},
  {"how to install vcpackage", "https://lindevs.com/install-vcpkg-on-ubuntu"}
};

// func without return
void print_reply(string reply) {
  cout << "[bot]: " << reply << "\n";
}

// func bot_reply (input: string user_text, return string)
// string bot_reply(string user_text)
void bot_reply(string user_text)
{
  bool found = false; // true = answer is found, false = wasn't found
  for (auto elem : questions_answers){
    // first version
    // if (user_text == elem.first) {
    //   return elem.second;
    // }

    // version with regular expressions
    // .*hello.* = any text with hello
    // icase = ignore case
    regex pattern(".*" + elem.first + ".*", regex_constants::icase);
    if (regex_match(user_text, pattern)) {
      found = true;
      // return elem.second;
      print_reply( elem.second);
    }
  }
  // return "I don't understand you";
  if (!found) {
  print_reply("I don't understand you");
  } 
}

int main()
{
  // std::cout - console output
  std::cout << "Welcome to chatbot\n";
  cout << "Hey there!\n"; // After added line 2 (using namespace std;)

  string question;

  while (question != "exit") {
    cout << "[user]: ";
    getline(std::cin, question); // read text from stdin (cin)
    bot_reply(question); // print bot answer
  } 
}
