include .env_Makefile

.DEFAULT_GOAL := all

# the same that .DEFAULT_GOAL for older version of make (<= 3.80)
# .PHONY: default
# default: clean;

.PHONY: all
all: build run clean

.PHONY: build
build:
	$(info -> Build:)
	mkdir -p ./bin
	g++ -o ./bin/${BINARY_NAME} ${PATH_TO_SOURCE}

.PHONY: run
run:
	$(info -> Run:)
	./bin/${BINARY_NAME}

.PHONY: clean
clean:
	$(info -> Clean:)
	rm ./bin/${BINARY_NAME}
	rmdir ./bin

.PHONY: messages
messages:
	$(info test info message)
	$(warning test warning message)
	$(error test error message)
